namespace Application.Client.EditPersonalData;

public class EditPersonalDataHandler
{
    private readonly IValidator<EditPersonalDataRequestDto> _requestValidator;

    public EditPersonalDataHandler(IValidator<EditPersonalDataRequestDto> requestValidator)
    {
        _requestValidator = requestValidator;
    }
    
    public async Task<EditPersonalDataResponseDto> Handle(EditPersonalDataRequestDto request)
    {
        _requestValidator.ValidateAndThrow(request);
        
        // ...
        // ...
        // ...

        return new EditPersonalDataResponseDto
        {
            Success = ...,
            MvdValidationSuccess = ...
        };
    }
}