namespace Application.Client.EditPersonalData;

/// <summary>
/// Данные профиля
/// </summary>
public class EditPersonalDataRequestDto
{
    /// <summary>
    /// Имя
    /// </summary>
    public string? FirstName { get; set; }

    /// <summary>
    /// Фамилия
    /// </summary>
    public string? LastName { get; set; }

    /// <summary>
    /// Отество
    /// </summary>
    public string? MiddleName { get; set; }

    /// <summary>
    /// День рождения
    /// </summary>
    public DateTime? BirthDay { get; set; }

    /// <summary>
    /// Серия паспорта
    /// </summary>
    public string? DocumentSeries { get; set; }

    /// <summary>
    /// Номер паспорта
    /// </summary>
    public string? DocumentNumber { get; set; }

    /// <summary>
    /// Код подразделения
    /// </summary>
    public string? DivisionCode { get; set; }

    /// <summary>
    /// Дата выдачи паспорта
    /// </summary>
    public DateTime? IssuedDate { get; set; }

    /// <summary>
    /// Адресс
    /// </summary>
    public string? Address { get; set; }
}