namespace Application.Client.EditPersonalData;

public class EditPersonalDataResponseDto
{
    public required bool Success { get; init; }
    public required bool MvdValidationSuccess { get; init; }
}