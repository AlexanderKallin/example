namespace Application.Client.GetPaymentTools;

public class GetPaymentToolsHandler
{
    private readonly IClientService _clientService;
    private readonly IValidator<GetPaymentToolsRequestDto> _requestValidator;

    public GetPaymentToolsHandler(IClientService clientService, IValidator<GetPaymentToolsRequestDto> requestValidator)
    {
        _clientService = clientService;
        _requestValidator = requestValidator;
    }
    
    public async Task<GetPaymentToolsResponseDto> Handle(GetPaymentToolsRequestDto request)
    {
        _requestValidator.ValidateAndThrow(request);

        //...
        //...
        //...

        return new GetPaymentToolsResponseDto
        {
            PaymentTools = ...
        };
    }
}