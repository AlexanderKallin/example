namespace Application.Client.GetPaymentTools;

public class GetPaymentToolsRequestDto
{
    public required long ClientId { get; init; }
}