namespace Application.Client.GetPaymentTools;

public class GetPaymentToolsResponseDto
{
    /// <summary>
    /// Инструменты оплаты
    /// </summary>
    public required List<PaymentTool> PaymentTools { get; init; }
}